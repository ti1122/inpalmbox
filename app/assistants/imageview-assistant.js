// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function ImageviewAssistant(imagePath) {
    this.imagePath = imagePath;
    
    // Menu
    this.appMenuAttr = { omitDefaultItems: true };
    this.appMenuModel = {
	visible: true,
	items: [
	    {
		label: $L("Close Image"),
		command: "close"
	    }
	]
    };

    // ImageViewer
    this.imageViewerAttr = {
        noExtractFS: false
    };
    this.imageViewerModel = {
        background: "black"  
    };
    
    this.imageViewerCtl = {};
}

ImageviewAssistant.prototype.setup = function() {
    this.controller.setupWidget(Mojo.Menu.appMenu, this.appMenuAttr, this.appMenuModel);
    this.controller.setupWidget("imageViewer", this.imageViewerAttr, this.imageViewerModel);
    
    this.imageViewerCtl = this.controller.get("imageViewer");
    this.imageViewerCtl.setStyle({
        width: Mojo.Environment.DeviceInfo.screenWidth + "px",
        height: Mojo.Environment.DeviceInfo.screenHeight + "px"
     });
    
};

ImageviewAssistant.prototype.ready = function() {
    this.imageViewerCtl.mojo.centerUrlProvided(this.imagePath);
//    this.imageViewerCtl.mojo.manualSize(this.controller.window.innerWidth, this.controller.window.innerHeight);
};

ImageviewAssistant.prototype.activate = function(event) {
};

ImageviewAssistant.prototype.deactivate = function(event) {
};

ImageviewAssistant.prototype.cleanup = function(event) {
};

ImageviewAssistant.prototype.handleCommand = function(event) {
    if (event.type != Mojo.Event.command) return;
        
    switch (event.command)
    {
        case "close":
            this.controller.stageController.popScene();
            break;
    }
};