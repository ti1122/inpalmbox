// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function LoginAssistant() {
	
	// Menu
    this.appMenuAttr = { omitDefaultItems: true };
    this.appMenuModel = {
		visible: true,
		items: [
			Mojo.Menu.editItem,
			{
				label: $L("Help"),
				command: Mojo.Menu.helpCmd
			}
		]
	};
	
    this.emailAttr = {
		focus: true, 
        hintText: "example@gmail.com",
		modelProperty: "value", 
        changeOnKeyPress: true,
        textCase: Mojo.Widget.steModeLowerCase,
        focusMode: Mojo.Widget.focusInsertMode,
		maxLength: 30
    };
    this.emailModel = {
        value: ""
    };
	
    this.passwordAttr = {
        hintText: $L("Required"),
		modelProperty: "value", 
        changeOnKeyPress: true,
        textCase: Mojo.Widget.steModeLowerCase,
        focusMode: Mojo.Widget.focusSelectMode,
		maxLength: 30,
        requiresEnterKey: true
    };
    this.passwordModel = {
        value: ""
    };
	
    this.loginButtonAttr = {
        label: $L("Log in to Dropbox"),
        type: Mojo.Widget.activityButton
    };
    this.loginButtonModel = {
        disabled: true
    };
	
    this.loginButtonModelChange = this.loginButtonModelChange.bind(this);
    this.loginButtonTap = this.loginButtonTap.bind(this);
	
    this.handleLoginSuccess = this.onLoginSuccess.bind(this);
    this.handleLoginFailure = this.onLoginFailure.bind(this);
}

LoginAssistant.prototype.setup = function() {
	this.controller.get("errorMessageText").update($L("Password invalid. Try again."));
	
    this.controller.setupWidget(Mojo.Menu.appMenu, this.appMenuAttr, this.appMenuModel);
    this.controller.setupWidget("email", this.emailAttr, this.emailModel);
    this.controller.setupWidget("password", this.passwordAttr, this.passwordModel);
    this.controller.setupWidget("loginButton", this.loginButtonAttr, this.loginButtonModel);
	
    this.emailCtl = this.controller.get("email");
    this.passwordCtl = this.controller.get("password");
    this.loginButtonCtl = this.controller.get("loginButton");
	this.errMessageCtl = this.controller.get("errorMessage");
};

LoginAssistant.prototype.activate = function(event) {
    // AddEvents
    this.emailCtl.addEventListener(Mojo.Event.propertyChange, this.loginButtonModelChange);
    this.passwordCtl.addEventListener(Mojo.Event.propertyChange, this.loginButtonModelChange);
    this.loginButtonCtl.addEventListener(Mojo.Event.tap, this.loginButtonTap);
};

LoginAssistant.prototype.deactivate = function(event) {
    // RemoveEvents
    this.emailCtl.removeEventListener(Mojo.Event.propertyChange, this.loginButtonModelChange);
    this.passwordCtl.removeEventListener(Mojo.Event.propertyChange, this.loginButtonModelChange);
    this.loginButtonCtl.removeEventListener(Mojo.Event.tap, this.loginButtonTap);
};

LoginAssistant.prototype.cleanup = function(event) {
    // CleanupEvents
};

LoginAssistant.prototype.handleCommand = function(event) {
    if (event.type == Mojo.Event.command && event.command == Mojo.Menu.helpCmd)
    {
        this.controller.stageController.pushScene({name: "support" ,disableSceneScroller: false});
    }
};

LoginAssistant.prototype.loginButtonModelChange = function() {
    this.loginButtonModel.disabled = this.emailModel.value.length === 0 || this.passwordModel.value.length === 0;
    this.controller.modelChanged(this.loginButtonModel);
};

LoginAssistant.prototype.loginButtonTap = function() {
    this.login();
};
    
LoginAssistant.prototype.login = function() {
    if (this.errMessageCtl.getStyle('display') != 'none')
    {
        this.errMessageCtl.setStyle({visibility:'hidden'});
    }
    else
    {
        this.errMessageCtl.hide();
        this.errMessageCtl.setStyle({visibility:'visible'});
    }
    this.loginButtonCtl.mojo.activate();
    Dropbox.api.login(this.emailModel.value, this.passwordModel.value, this.handleLoginSuccess, this.handleLoginFailure);
};

LoginAssistant.prototype.onLoginSuccess = function(){
    INPalmBox.prefs.setAuth(Dropbox.OAuth.accessToken, Dropbox.OAuth.tokenSecret);
    this.controller.stageController.swapScene("main", "/");
};

LoginAssistant.prototype.onLoginFailure = function(status){
	this.errMessageCtl.show();
	this.errMessageCtl.setStyle({visibility: "visible"});
	this.loginButtonCtl.mojo.deactivate();
	this.passwordCtl.mojo.focus();
};
