// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function SupportAssistant() {
    this.appInfo = Mojo.Locale.readStringTable("appinfo.json");
    if (Mojo.Locale._objectIsEmpty(this.appInfo))
    {
        this.appInfo = Mojo.appInfo;
    }
}

SupportAssistant.prototype.setup = function() {
    this.controller.get("helpTitle").update($L("Help"));
    
    var support, resources;
    this.controller.setupWidget(Mojo.Menu.appMenu, this.attributes = {
            omitDefaultItems: true
        }, this.model = {
            visible: true,
            items: [ Mojo.Menu.editItem ]
        }
    );
//    var iconDiv = this.controller.get("app-support-info-app-icon");
//    iconDiv.style["background-image"] = "url(" + this.appInfo.icon + ")";

    this.controller.get("app-support-info-app-name").innerHTML = this.appInfo.title || "";
    this.controller.get("app-support-info-app-details").innerHTML = new Template($LL("#{version} by #{vendor}")).evaluate(this.appInfo);
    this.helpList = this.controller.get("app-support-info-help-list");
    this.supportList = this.controller.get("app-support-info-support-list");

    var supportitems = [];
    support = this.appInfo.support || {};
    resources = support.resources || [];
    if (support.url) {
        Mojo.Log.info("supporturl");
        supportitems.push({
            text: $L("Support Website"),
            detail: $L(support.url),
            type:"web"
        });
    }
    if (support.email) {
        supportitems.push({
            text: $L("Send Email"),
            address: support.email.address,
            subject:support.email.subject,
            type:"email"
        });
    }
    if (support.phone) {
        supportitems.push({
            text: $L(support.phone),
            detail: $L(support.phone),
            type:"phone"
        });
    }
    if (this.appInfo.twitter !== undefined && this.appInfo.twitter) {
        supportitems.push({
            text: $LL("Twitter"),
            detail: $L(this.appInfo.twitter),
            type:"twitter"
        });
    }

    try {
        var helpitems = [];
        var j = 0;
        for (j = 0; j < resources.length; j++) {
            if (resources[j].type === "web") {
                helpitems.push({
                    text: resources[j].label,
                    detail: resources[j].url,
                    type: "web"
                });
            } else if (resources[j].type === "scene") {
                helpitems.push({
                    text: resources[j].label,
                    detail: resources[j].sceneName,
                    appIcon: (this.appInfo.smallicon || this.appInfo.icon),
                    type: "scene"
                });
            }
        }
        if (resources.length > 0) {
            this.controller.setupWidget(this.helpList.id, {
                itemTemplate: "support/listitem",
                listTemplate: "support/listcontainer",
                onItemRendered: this._renderHelpList,
                swipeToDelete: false
            }, {
                listTitle: $LL("Help"),
                items: helpitems
            });
        } else {
            this.helpList.remove();
            this.helpList = undefined;
        }
    } catch(e) {
        Mojo.Log.error(e);
    }
    this.controller.setupWidget(this.supportList.id, {
        itemTemplate: "support/listitem",
        listTemplate: "support/listcontainer",
        swipeToDelete: false
    }, {
        listTitle: $LL("Support"),
        items : supportitems
    });
    
    this.handleListTap = this.handleListTap.bind(this);
    if (this.helpList) {
        this.controller.listen(this.helpList, Mojo.Event.listTap, this.handleListTap);		
    }
    this.controller.listen(this.supportList, Mojo.Event.listTap, this.handleListTap);
    
    this.controller.get("app-support-info-copyright").innerHTML = this.appInfo.copyright || "";
    
};

SupportAssistant.prototype._renderHelpList = function(widget, model, node) {
    if (model.appIcon) {
        node = node.querySelector("div.app-support-info.scene");
        if (node) {
            node.style["background-image"] = "url(" + model.appIcon + ")";
        }
    }
};

SupportAssistant.prototype.handleListTap = function(event) {
    switch (event.item.type) {
        case "web":
            this.controller.serviceRequest("palm://com.palm.applicationManager", {
                method:"open",
                parameters: {
                    id: "com.palm.app.browser",
                    params: {
                        target: event.item.detail
                    }
                }
            });
            break;
        case "twitter":
            this.controller.serviceRequest("palm://com.palm.applicationManager", {
                method:"open",
                parameters:  {
                    id: "com.palm.app.browser",
                    params: {
                        target: "http://m.twitter.com/" + event.item.detail
                    }
                }
            });		   
            break;
        case "email":
            this.controller.serviceRequest("palm://com.palm.applicationManager", {
                method:"open",
                parameters: {
                    target: "mailto:" + event.item.address + "?subject=["  + this.appInfo.title + "] " + event.item.subject
                }
            });
            break;
        case "phone":
            this.controller.serviceRequest("palm://com.palm.applicationManager", {
                method:"open",
                parameters: {
                    target: "tel://" + event.item.detail
                }
            });
            break;
        case "scene":
            this.controller.stageController.pushScene(event.item.detail);
            break;
        default:
            Mojo.Log.error("Unknown support item type: '" + event.item.type + "'");
            break;     
    }
};

SupportAssistant.prototype.cleanup = function(event) {
    this.controller.stopListening(this.helpList, Mojo.Event.listTap, this.handleListTap);		
    this.controller.stopListening(this.supportList, Mojo.Event.listTap, this.handleListTap);
};

