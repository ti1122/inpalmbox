// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function PreferencesAssistant() {
    
    // Menu
    this.appMenuAttr = { omitDefaultItems: true };
    this.appMenuModel = {
		visible: true,
		items: [
			Mojo.Menu.editItem,
			{
				label: $L("Help"),
				command: Mojo.Menu.helpCmd
			}
		]
    };

    this.spinnerModel = {
        spinning: true
    };
    
    this.clearButtonModel = {
		label: $L("Clear cache"),
		buttonClass: "negative"
    };

    this.unlinkButtonModel = {
		label: $L("Unlink device from Dropbox"),
		buttonClass: "negative"
    };
    
    this.clearButtonTap = this.clearButtonTap.bind(this);
    this.unlinkButtonTap = this.unlinkButtonTap.bind(this);
}

PreferencesAssistant.prototype.setup = function() {
    this.controller.get("preferencesTitle").update($L("Preferences"));
	
    this.controller.setupWidget(Mojo.Menu.appMenu, this.appMenuAttr, this.appMenuModel);
    this.controller.setupWidget("email-spinner", undefined, this.spinnerModel);
    this.controller.setupWidget("space-used-spinner", undefined, this.spinnerModel);
    this.controller.setupWidget("clearButton", undefined, this.clearButtonModel);
    this.controller.setupWidget("unlinkButton", undefined, this.unlinkButtonModel);

	this.emailCtl = this.controller.get("email");
	this.spaceUsedCtl = this.controller.get("space-used");
    this.clearButtonCtl = this.controller.get("clearButton");
    this.unlinkButtonCtl = this.controller.get("unlinkButton");
    
    this.accountInfo();
};

PreferencesAssistant.prototype.activate = function(event) {
    // AddEvents
    this.clearButtonCtl.addEventListener(Mojo.Event.tap, this.clearButtonTap);
    this.unlinkButtonCtl.addEventListener(Mojo.Event.tap, this.unlinkButtonTap);
};

PreferencesAssistant.prototype.deactivate = function(event) {
    // RemoveEvents
    this.clearButtonCtl.removeEventListener(Mojo.Event.tap, this.clearButtonTap);
    this.unlinkButtonCtl.removeEventListener(Mojo.Event.tap, this.unlinkButtonTap);
};

PreferencesAssistant.prototype.cleanup = function(event) {
    // CleanupEvents
};

PreferencesAssistant.prototype.handleCommand = function(event) {
    if (event.type == Mojo.Event.command && event.command == Mojo.Menu.helpCmd)
    {
        this.controller.stageController.pushScene({name:"support", disableSceneScroller: false});
    }
};

PreferencesAssistant.prototype.clearButtonTap = function() {
    var self = this;
    this.controller.showAlertDialog({
		title: $L("Clear cache"),
		message: $L("This will remove any downloaded cache data from this Device. Are you sure."),
		choices:[
			 {label:$L("Clear"), value: "clear", type: "negative"},  
			 {label:$L("Cancel"), value: "cancel", type: "dismiss"}    
		],
		onChoose: function(value)
		{
			if( value != "clear" ) return;
			INPalmBox.cacheManager.clear();
		}
    });
};

PreferencesAssistant.prototype.unlinkButtonTap = function() {
    var self = this;
    this.controller.showAlertDialog({
		title: $L("Unlink device from Dropbox"),
		message: $L("This will sign you out of Dropbox and remove your Favorites from this Device. Are you sure."),
		choices:[
			 {label:$L("Unlink device"), value: "unlink", type: "negative"},  
			 {label:$L("Cancel"), value: "cancel", type: "dismiss"}    
		],
		onChoose: function(value)
		{
			if (value != "unlink")
			{
				return;
			}
			
			INPalmBox.prefs.clear();
			INPalmBox.cacheManager.clear();
			
			self.controller.stageController.popScenesTo();
			self.controller.stageController.pushScene("login");
		}
    });
};

PreferencesAssistant.prototype.accountInfo = function() {
    var self = this;
    Dropbox.api.accountInfo(
		function onSuccess(info)
		{
			self.spinnerModel.spinning = false;
			self.controller.modelChanged(self.spinnerModel);
			
			self.emailCtl.update(info.email);
			self.emailCtl.show();
			
			var quota = info.quota_info;
			var percUsed = (quota.normal + quota.shared) / quota.quota * 100;
			self.spaceUsedCtl.update(percUsed.toFixed(1) + "% of " + INPalmBox.formatter.formatByteSize(quota.quota, null, 1));
			self.spaceUsedCtl.show();
		},
		function onFailure()
		{
		}
    );
};

