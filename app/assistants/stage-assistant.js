// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function StageAssistant() {
	Dropbox.OAuth.consumerKey = INPalmBox.prefs.getConsumerKey();
	Dropbox.OAuth.consumerSecret = INPalmBox.prefs.getConsumerSecret();
    var auth = INPalmBox.prefs.getAuth();
    Dropbox.OAuth.accessToken = auth.token;
    Dropbox.OAuth.tokenSecret = auth.secret;
}

StageAssistant.prototype.setup = function() {
	if (Dropbox.OAuth.isLoginNecessary())
    {
        //ログイン処理
        this.controller.pushScene("login");
    }
    else
    {
        //mainへ移動
        this.controller.pushScene("main", "/");
    }
};

StageAssistant.prototype.cleanup = function(event) {
Mojo.Log.info("** stage-cleanup");
    downloadManager.cancelAllDownloads();
    uploadManager.cancelAllUploads();
};

