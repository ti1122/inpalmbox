// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function InputDialogAssistant(params) {
	this.onAccept = params.onAccept;
	this.controller = params.sceneAssistant.controller;
	this.title = params.title;
	this.message = params.message;
	this.initValue = params.init;
	
	this.inputAttr = {
		autoFocus: true,
		limitResize: true,
		autoReplace: false,
		textCase: Mojo.Widget.steModeLowerCase,
		enterSubmits: false
	};
	this.inputModel = {
		value: this.initValue
	};
	
	this.okButtonAttr = {
		type: Mojo.Widget.defaultButton
	};
	this.okButtonModel = {
		label: $L("OK"),
		disabled: false,
		buttonClass: "affirmative"
	};

	this.cancelButtonAttr = {
		type: Mojo.Widget.defaultButton
	};
	this.cancelButtonModel = {
		label: $L("Cancel"),
		disabled: false
	};
	
	this.inputCtl = {};
	this.okButtonCtl = {};
	this.cancelButtonCtl = {};
}

InputDialogAssistant.prototype.setup = function(widget) {
	this.widget = widget;
	this.controller.get("title").update("  " + this.title);
	this.controller.get("message").update(this.message);
	
	this.controller.setupWidget("input", this.inputAttr, this.inputModel);
	this.controller.setupWidget("okButton", this.okButtonAttr, this.okButtonModel);
	this.controller.setupWidget("cancelButton", this.cancelButtonAttr, this.cancelButtonModel);
	
	this.inputCtl = this.controller.get("input");
	this.okButtonCtl = this.controller.get("okButton");
	this.cancelButtonCtl = this.controller.get("cancelButton");
};

InputDialogAssistant.prototype.handleFilter = function(event) {
	if (!this.isValidChar(event.keyCode))
	{
		event.stop();
		this.inputModel.value = this.inputModel.value.replace(String.fromCharCode(event.keyCode), "");
	}
};

InputDialogAssistant.prototype.isValidChar = function(keycode) {
	//valid character range: 32-255
	//disallowed characters: \ / : * ? " < > |
	return (keycode>=32 && keycode<=255 && keycode!=92 && keycode!=47  && keycode!=58
			&& keycode!=42 && keycode!=63 && keycode!=34 && keycode!=60  && keycode!=62
			&& keycode!=124);
};

InputDialogAssistant.prototype.handleAccept = function(){
	var result = this.inputModel.value.trim();
	if (result == "")
	{
		this.inputCtl.mojo.focus();
	}
	else
	{
		this.onAccept(result);
		delete this.onAccept;
		this.widget.mojo.close();
	}
};

InputDialogAssistant.prototype.handleDismiss = function() {
	this.widget.mojo.close();
};

InputDialogAssistant.prototype.activate = function(event) {
	this.handleFilter = this.handleFilter.bindAsEventListener(this);
	this.handleAccept = this.handleAccept.bindAsEventListener(this);
	this.handleDismiss = this.handleDismiss.bindAsEventListener(this);

	this.inputCtl.addEventListener("keypress", this.handleFilter);
	this.okButtonCtl.addEventListener(Mojo.Event.tap, this.handleAccept);
	this.cancelButtonCtl.addEventListener(Mojo.Event.tap, this.handleDismiss);
};


InputDialogAssistant.prototype.deactivate = function(event) {
	this.inputCtl.removeEventListener("keypress", this.handleFilter);
	this.okButtonCtl.removeEventListener(Mojo.Event.tap, this.handleAccept);
	this.cancelButtonCtl.removeEventListener(Mojo.Event.tap, this.handleDismiss);
};

InputDialogAssistant.prototype.cleanup = function(event) {
	this.okButtonCtl.removeEventListener(Mojo.Event.tap, this.handleAccept);
	this.cancelButtonCtl.removeEventListener(Mojo.Event.tap, this.handleDismiss);
};
