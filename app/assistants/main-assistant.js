// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
function MainAssistant(path) {
    
    this.handleItemTap = this.handleItemTap.bind(this);
    this.handleActionTap = this.handleActionTap.bind(this);
    this.handleItemDelete = this.handleItemDelete.bind(this);
    this.handleUploadItemTap = this.handleUploadItemTap.bind(this);
    
    this.handleContentsSuccess = this.onContentsSuccess.bind(this);
    this.handleContentsFailure = this.onContentsFailure.bind(this);
    this.handleSpinnerStart = this.onSpinnerStart.bind(this);
    
    var self = this;
    
    // Dropbox list
    this.dropboxList = new DropboxList(path);
    this.dropboxListAttr = {
        itemsProperty: "contents",
        itemTemplate: "main/main-item",
        formatters: {
            is_dir: this.listFormatterIs_dir.bind(this),
            bytes: INPalmBox.formatter.formatByteSize,
            modified: INPalmBox.formatter.fomatDate
        },
        swipeToDelete: true,
        onItemRendered: this.handleListItemRendered.bind(this),
        actions: [
              {action: "open", icon: "images/action/open.png"},
              {action: "download", icon: "images/action/download.png"},
              {action: "messaging", icon: "images/action/messaging.png"},
              {action: "email", icon: "images/action/email.png"},
              {action: "twitter", icon: "images/action/twitter.png"}
            ]
    };
    
    // Upload list
    this.uploadList = new UploadList(this.dropboxList.path);
    this.uploadListAttr = {
        itemsProperty: "contents",
        itemTemplate: "main/main-upload-item",
        listTemplate: "main/main-upload-list-container",
        onItemRendered: this.handleUpListItemRendered.bind(this)
    };
    
    this.cancelAllDownloads = "cancel_all_downloads";
    this.cancelAllUploads = "cancel_all_uploads";
    // Menu
    this.appMenuAttr = { omitDefaultItems: true };
    this.appMenuModel = {
        visible: true,
        items: [
            Mojo.Menu.editItem,
            {
                label: $L("Preferences"), command: Mojo.Menu.prefsCmd
            },
            {
                label: $L("Cancel All Downloads"), command: this.cancelAllDownloads
            },
            {
                label: $L("Cancel All Uploads"), command: this.cancelAllUploads
            },
            {
                label: $L("Help"), command: Mojo.Menu.helpCmd
            }
        ]
    };
    
    var leftHeaderButton = null;
    if (this.dropboxList.path === "/")
    {
        leftHeaderButton = {label: $L("")};
    }
    else
    {
        leftHeaderButton = {label: $L("Back"), icon: "back", command: "back"};
    }
    this.headerMenuModel = {
        items: [
            {label: "HeaderMenu", items: [
                leftHeaderButton,
                {label: this.dropboxList.title, width: 200, command: "header_tap"},
                {label: $L("Upload"), icon: "new", command: "upload"}
            ]}
        ]
    };
    
    this.refreshCmd = {label: $L("Refresh"), icon: "refresh", command:"refresh"};
    this.cmdMenuModel = {
        visible: true,
        items: [
            {items:[
            ]},
            {items:[
                this.refreshCmd
            ]}
        ]
    };
    
    this.refreshSpinnerModel = {spinning: true};
}

MainAssistant.prototype.setup = function() {
    this.controller.setupWidget(Mojo.Menu.appMenu, this.appMenuAttr, this.appMenuModel);
    this.controller.setupWidget(Mojo.Menu.viewMenu, undefined, this.headerMenuModel);
    this.controller.setupWidget(Mojo.Menu.commandMenu, undefined, this.cmdMenuModel);
    this.controller.setupWidget("refresh-spinner", {spinnerSize: Mojo.Widget.spinnerSmall}, this.refreshSpinnerModel);
    this.controller.setupWidget("dropbox-list", this.dropboxListAttr, this.dropboxList);
    this.controller.setupWidget("upload-list", this.uploadListAttr, this.uploadList);
    
    this.dropboxListCtl = this.controller.get("dropbox-list");
    this.uploadListCtl = this.controller.get("upload-list");
    
    this.uploadList.refresh();
    this.refresh(true);

};

MainAssistant.prototype.activate = function(event) {
    // AddEvents
    this.dropboxListCtl.addEventListener(Mojo.Event.listTap, this.handleItemTap);
    this.dropboxListCtl.addEventListener(Mojo.Event.customActionTap, this.handleActionTap);
    this.dropboxListCtl.addEventListener(Mojo.Event.listDelete, this.handleItemDelete);
    this.uploadListCtl.addEventListener(Mojo.Event.listTap, this.handleUploadItemTap);
};

MainAssistant.prototype.deactivate = function(event) {
    // RemoveEvents
    this.dropboxListCtl.removeEventListener(Mojo.Event.listTap, this.handleItemTap);
    this.dropboxListCtl.removeEventListener(Mojo.Event.customActionTap, this.handleActionTap);
    this.dropboxListCtl.removeEventListener(Mojo.Event.listDelete, this.handleItemDelete);
    this.uploadListCtl.removeEventListener(Mojo.Event.listTap, this.handleUploadItemTap);
};

MainAssistant.prototype.cleanup = function(event) {
    // CleanupEvents
    var i = 0;
    for (i = 0; i < this.dropboxList.contents.length; i++)
    {
        var item = this.dropboxList.contents[i];
        var loader = downloadManager.getDownloader(item.hash);
        if (loader)
        {
            this.cleanupDownloader(loader);
        }
    }
    
    for (i = 0; i < this.uploadList.contents.length; i++)
    {
        item = this.uploadList.contents[i];
        loader = uploadManager.getUploader(item.key);
        if (loader)
        {
            this.cleanupUploader(loader);
        }
    }
};

MainAssistant.prototype.listFormatterIs_dir = function(is_dir, itemModel) {
    return is_dir ? "dropbox-item-dir" : "dropbox-item-file";
}

MainAssistant.prototype.onContentsSuccess = function()
{
    this.controller.modelChanged(this.dropboxList);
    this.onSpinnerStop();
};

MainAssistant.prototype.onContentsFailure = function()
{
    this.controller.modelChanged(this.dropboxList);
    this.onSpinnerStop();
    Mojo.Controller.getAppController().showBanner($L("Check your internet connection."), null);
};

MainAssistant.prototype.handleCommand = function(event) {
    Mojo.Log.info("event_type: %s, %s", event.type, event.command);
    if (event.type !== Mojo.Event.command)
    {
        return;
    }
    
    var commandFunc = {};
    commandFunc[Mojo.Menu.prefsCmd] = function(){
        this.controller.stageController.pushScene("preferences");
    }.bind(this);
    commandFunc[Mojo.Menu.helpCmd] = function(){
        this.controller.stageController.pushScene({name:"support" ,disableSceneScroller: false});
    }.bind(this);
    commandFunc["back"] = function(){
        if (this.dropboxList.path === "/")
        {
            return;
        }
        
        if (this.controller.stageController.parentSceneAssistant(this))
        {
            this.controller.stageController.popScene();
        }
        else
        {
            var previousPath = this.dropboxList.path.slice(0, this.dropboxList.path.lastIndexOf("/"));
            this.controller.stageController.swapScene("main", previousPath);
        }
    }.bind(this);
    commandFunc["upload"] = function(){
        var near = event.originalEvent && event.originalEvent.target;
        this.controller.popupSubmenu({
            onChoose: this.uploadMenuChoose.bind(this),
            placeNear: near,
            items: [
                {
                    label: $L("Upload File"), command: "uploadFile"
                },
                {
                    label: $L("Create Folder"), command: "createFolder"
                }
            ]
        });
    }.bind(this);
    commandFunc["refresh"] = function(){
        this.refresh(false);
    }.bind(this);
    commandFunc["header_tap"] = function(){
        this.scrollToTop();
    }.bind(this);
    commandFunc[this.cancelAllDownloads] = function() {
        downloadManager.cancelAllDownloads();
    }.bind(this);
    commandFunc[this.cancelAllUploads] = function() {
        uploadManager.cancelAllUploads();
    }.bind(this);
    
    var func = commandFunc[event.command];
    if (func)
    {
        func();
    }
    
};

MainAssistant.prototype.handleListItemRendered = function(widget, item, node) {
    if (!downloadManager.hasDownloader(item.hash))
    {
        return;
    }
    
    var loader = downloadManager.getDownloader(item.hash);
    if (!loader.downloadStartEvent)
    {
        this.initDownloader(loader);
    }
    var progId = "progId-" + loader.downloadInfo.key;
    
    this.controller.setupWidget(progId, {modelProperty: "progressValue"}, loader.downloadInfo);
    var infoNode = node.querySelector("div[name=item-info-lower]");
    this.controller.update(infoNode,
        Mojo.View.render({
            object: {id: progId},
            template: "main/main-item-info-progress"
        })
    );
};

MainAssistant.prototype.handleUpListItemRendered = function(widget, item, node) {
    if (!uploadManager.hasUploader(item.key))
    {
        return;
    }
    
    var loader = uploadManager.getUploader(item.key);
    if (!loader.uploadStartEvent)
    {
        this.initUploader(loader);
    }
};

MainAssistant.prototype.handleItemTap = function(event) {
    var item = event.item;
    
    if (item.is_dir)
    {
        this.dropboxListCtl.mojo.hideActionBar();
        this.controller.stageController.pushScene("main", item.path);
    }
    else if (item.path)
    {
        var loader = downloadManager.getDownloader(item.hash);
        if (loader)
        {
            loader.cancelDownload();
            return;
        }
        
        if (this.dropboxListCtl.mojo.getActionBarIndex() === event.index)
        {
            this.dropboxListCtl.mojo.hideActionBar();
        }
        else
        {
            var isImageFile = this.imageMimeType(item.mime_type);
            this.dropboxListCtl.mojo.setActionEnabled(2, isImageFile);
            this.dropboxListCtl.mojo.setActionEnabled(4, isImageFile);
            this.dropboxListCtl.mojo.showActionBar(event.index);
        }
    }
};

MainAssistant.prototype.handleActionTap = function(event) {
    
    var item = event.item;
    
    var actionFunc = {
        "open": function()
        {
            this.dropboxListCtl.mojo.hideActionBar();
            this.downloadFile(item, true, this.openFile);
        }.bind(this),
        "download": function()
        {
            this.dropboxListCtl.mojo.hideActionBar();
            this.downloadFile(item, false, this.downloadBanner);
        }.bind(this),
        "messaging": function()
        {
            this.dropboxListCtl.mojo.hideActionBar();
            this.downloadFile(item, true, this.sendToMessaging);
        }.bind(this),
        "email": function()
        {
            this.dropboxListCtl.mojo.hideActionBar();
            this.downloadFile(item, true, this.sendToEmail);
        }.bind(this),
        "twitter": function()
        {
            this.dropboxListCtl.mojo.hideActionBar();
            this.downloadFile(item, true, this.sendToTsubumoca);
        }.bind(this)
    };
    
    var func = actionFunc[event.action];
    if (func)
    {
        func();
    }
};

MainAssistant.prototype.downloadBanner = function(loader, info) {
    
    var fileName = loader.getItem().name;
    Mojo.Controller.getAppController().showBanner($L("Downloaded: ") + fileName, null);
};

MainAssistant.prototype.openFile = function(loader, info) {
    
    var self = this;
    
    var target = info.target;
    var mime_type = info.mime_type;
    this.controller.serviceRequest(
        "palm://com.palm.applicationManager", {
            method: "open",
            parameters: {
               target: "file://" + target
            },
            onSuccess: function() {},
            onFailure: function() {
                Mojo.Log.info("target:%s mime:%s", target, mime_type);
                if (self.imageMimeType(mime_type))
                {
                    self.controller.stageController.pushScene("imageview", target);
                }
                else
                {
                    Mojo.Controller.getAppController().showBanner($L("This file type can't be opened."), null);
                }
            }
        }
    );
    
};

MainAssistant.prototype.sendToMessaging = function(loader, info) {
    
    var target = info.target;
    this.controller.serviceRequest(
        "palm://com.palm.applicationManager", {
            method: 'open',
            parameters: {
                id: "com.palm.app.messaging",
                params: {
                    messageText: "",
                    attachment: target
                }
            }
        }
    );
};

MainAssistant.prototype.sendToEmail = function(loader, info) {
    
    var target = info.target;
    this.controller.serviceRequest(
        "palm://com.palm.applicationManager", {
            method: 'open',
            parameters: {
                id: "com.palm.app.email",
                params: {
                    summary: "",
                    text: "",
                    attachments: [{fullPath: target}]
                }
            }
        }
    );
};

MainAssistant.prototype.sendToTsubumoca = function(loader, info) {
    
    var self = this;
    var target = info.target;
    this.controller.serviceRequest(
        "palm://com.palm.applicationManager", {
            method: "open",
            parameters: {
                id: "com.fc2.blog23.zawakei.app.tsubumoca",
                params: {
                    action: "tweet", 
                    tweet: "#inpalmbox",
                    attachment: target
                }
            },
            onFailure: function() {
                self.controller.showAlertDialog( {
                    title: $L("Tsubumoca is not installed"),
                    message: $L("Tsubumoca is not installed. Would you like to install it?"),
                    choices:[
                        {label:$L("Yes"), value:"yes", type:"affirmative"},
                        {label:$L("No"), value:"no", type:"dismissal"}
                    ],
                    onChoose: function(value){
                        if ("yes" == value)
                        {
                            self.controller.serviceRequest(
                                "palm://com.palm.applicationManager", {
                                    method:"open",
                                    parameters: {
                                        id: "com.palm.app.browser",
                                        params: {
                                            target: "http://zawakei.web.fc2.com/software/tsubumoca/"
                                        }
                                    }
                                }
                            );
                        }
                    }
                });
            }
        }
    );
};

MainAssistant.prototype.handleUploadItemTap = function(event) {
    var item = event.item;
    
    var self = this;
    if (item)
    {
        var loader = uploadManager.getUploader(item.key);
        if (loader)
        {
            loader.cancelUpload();
        }
    }
};

MainAssistant.prototype.imageMimeType = function(mimeType) {
    var mimeTypes = ["image/x-ms-bmp", "image/bmp", "image/jpeg", "image/png"];
    return (mimeTypes.indexOf(mimeType) != -1);
};

MainAssistant.prototype.refresh = function(isCache) {
    this.dropboxList.refresh(isCache, this.handleSpinnerStart, this.handleContentsSuccess, this.handleContentsFailure);
};

MainAssistant.prototype.onSpinnerStart = function() {
    this.refreshCmd = {template: "templates/commandMenuSpinner", disabled: true};
    this.cmdMenuModel.items[1] = this.refreshCmd;
    this.controller.modelChanged(this.cmdMenuModel);	
};

MainAssistant.prototype.onSpinnerStop = function() {
    this.refreshCmd = {icon: "refresh", command: "refresh"};
    this.cmdMenuModel.items[1] = this.refreshCmd;
    this.controller.modelChanged(this.cmdMenuModel);
};

MainAssistant.prototype.initDownloader = function(loader) {
    
    var fileDownloadStart = function(loader, info) {
    Mojo.Log.info("Download Start");
        this.controller.modelChanged(this.dropboxList);
    };
    
    var fileDownloading = function(loader, info) {
        this.controller.modelChanged(info);
        this.controller.get("progId-" + info.key + "-dlBytes").update(INPalmBox.formatter.formatByteSize(info.receivedBytes));
    };
    
    var fileDownloaded = function(loader, info) {
    Mojo.Log.info("Downloaded");
        this.controller.modelChanged(info);
        this.controller.get("progId-" + info.key + "-dlBytes").update(INPalmBox.formatter.formatByteSize(info.totalBytes));
        
        var self = this;
        setTimeout( function () {
            self.controller.modelChanged(self.dropboxList);
        }, 1000);
    };
    
    var fileDownloadFailure = function(loader, info, resp) {
    Mojo.Log.info("Download Failure");
        if (resp.aborted)
        {
            Mojo.Controller.getAppController().showBanner($L("DL canceled: ") + info.name, null);
        }
        else
        {
            Mojo.Controller.getAppController().showBanner($L("DL failed: ") + info.name, null);
        }
        this.controller.modelChanged(this.dropboxList);
    };

    loader.downloadStartEvent = fileDownloadStart.bind(this);
    loader.downloadingEvent = fileDownloading.bind(this);
    loader.downloadedEvent = fileDownloaded.bind(this);
    loader.downloadFailureEvent = fileDownloadFailure.bind(this);
}

MainAssistant.prototype.cleanupDownloader = function(loader) {
    loader.downloadStartEvent = null;
    loader.downloadingEvent = null;
    loader.downloadedEvent = null;
    loader.downloadFailureEvent = null;
};

MainAssistant.prototype.downloadFile = function(item, isCache, afterDownloadHandler) {
    
    var self = this;
    var afterDownload = {};
    if (afterDownloadHandler)
    {
        afterDownload = afterDownloadHandler.bind(this);
    }
    var downloadHandler = function onFailure() {
        var loader = downloadManager.getDownloader(item.hash, item);
        self.initDownloader(loader);
        loader.downloadFile(isCache);
        loader.afterDownloadEvent = afterDownload;
    };
    
    if (isCache)
    {
        downloadManager.getCacheInfo(item,
            function onSuccess(file) {
                afterDownload(null, file);
            },
            downloadHandler
        );
    }
    else
    {
        downloadHandler();
    }
};

MainAssistant.prototype.handleItemDelete = function(event) {
    var item = event.item;
    
    var loader = downloadManager.getDownloader(item.hash);
    if (loader)
    {
        loader.cancelDownload();
    }
    this.dropboxList.deleteItem(item);
};

MainAssistant.prototype.uploadMenuChoose = function(command) {
    switch (command)
    {
        case "uploadFile":
            Mojo.FilePicker.pickFile({
                defaultKind: "image",
                onSelect: this.fileUpload.bind(this),
                onCancel: function()
                {
                    Mojo.Log.info("Returning from selection wizard with cancel");
                }
            }, this.controller.stageController);
            this.scrollToTop();
            break;
        
        case "createFolder":
            this.createFolder();
            break;
    }
};

MainAssistant.prototype.scrollToTop = function() {
    if (0 < this.uploadList.contents.length)
    {
        this.uploadListCtl.mojo.revealItem(this.uploadList.contents.length - 1, false);
    }
    else if (0 < this.dropboxList.contents.length)
    {
        this.dropboxListCtl.mojo.revealItem(0, false);
    }
}

MainAssistant.prototype.initUploader = function(loader) {
    
    var fileUploadStart = function(loader, info) {
    Mojo.Log.info("Upload Start");
        this.onUploadListModelChenged();
    };
    
    var fileUploading = function(loader, info) {
    };
    
    var fileUploaded = function(loader, info) {
    Mojo.Log.info("Uploaded");
    
        var self = this;
        setTimeout( function () {
            self.onUploadListModelChenged();
            self.refresh(false);
            Mojo.Controller.getAppController().showBanner($L("Uploaded: ") + info.name, null);
        }, 1000);
    };
    
    var fileUploadFailure = function(loader, info, resp) {
    Mojo.Log.info("Upload Failure:%j", resp);
    
        var self = this;
        setTimeout( function () {
            self.onUploadListModelChenged();
            if (resp.aborted)
            {
                Mojo.Controller.getAppController().showBanner($L("Up canceled: ") + info.name, null);
            }
            else
            {
                Mojo.Controller.getAppController().showBanner($L("Up failed: ") + info.name, null);
            }
        }, 200);
    };

    loader.uploadStartEvent = fileUploadStart.bind(this);
    loader.uploadingEvent = fileUploading.bind(this);
    loader.uploadedEvent = fileUploaded.bind(this);
    loader.uploadFailureEvent = fileUploadFailure.bind(this);
};

MainAssistant.prototype.cleanupUploader = function(loader) {
    loader.uploadStartEvent = null;
    loader.uploadingEvent = null;
    loader.uploadedEvent = null;
    loader.uploadFailureEvent = null;
};

MainAssistant.prototype.fileUpload = function(file) {
    
    if (!file.hash)
    {
        file.hash = hex_sha1(Object.toJSON(file));
    }
    file.uploadUrl = this.dropboxList.path;
    
    var loader = uploadManager.getUploader(file.hash, file);
    this.initUploader(loader);

    loader.uploadFile();
    return loader.uploadInfo;
    
};

MainAssistant.prototype.onUploadListModelChenged = function() {
    this.uploadList.refresh();
    this.controller.modelChanged(this.uploadList);
};

MainAssistant.prototype.createFolder = function() {
    var self = this;
    this.controller.showDialog({
        template: 'templates/input-dialog-popup',
        assistant: new InputDialogAssistant({
            sceneAssistant: this,
            title: $L("Create Folder"),
            message: $L("New folder's name:"),
            init: "",
            onAccept: function(value)
            {
                Mojo.Log.info("New folder's name:%s", value);
                self.dropboxList.createFolder(value,
                    function onSuccess()
                    {
                        self.refresh();
                    },
                    function onFailure()
                    {
                        Mojo.Controller.getAppController().showBanner($L("Create folder failed."), null);
                    }
                );
            }
        })
    });
};
