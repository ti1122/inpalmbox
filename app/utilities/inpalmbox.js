// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
INPalmBox = {};

INPalmBox.prefs = prefs;
INPalmBox.cacheManager = cacheManager;

INPalmBox.formatter = {
    formatByteSize: function(bytes, itemModel, digits)
    {
        digits = (typeof digits == "number") ? digits : 2;
        
        var kb = 1024;
        var mb = kb * kb;
        var gb = mb * kb;
        
        if (gb <= bytes)
        {
            return (bytes / gb).toFixed(digits) + "GB";
        }
            
        if (mb <= bytes)
        {
            return (bytes / mb).toFixed(digits) + "MB";
        }
            
        if (kb <= bytes)
        {
            return (bytes / kb).toFixed(digits) + "KB";
        }
        
        return bytes + " bytes";
    },
    
    fomatDate: function(date, itemModel)
    {
        var fdate = new Date(date);
        return Mojo.Format.formatDate(fdate, "default");
    }
};
