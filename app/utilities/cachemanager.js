// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var cacheManager = function() {
    var that = {};
    
    // キャッシュDB
    var _cacheListDepot = new Mojo.Depot({name: "ext:cache-list"});
    var _cacheFileDepot = new Mojo.Depot({name: "ext:cache-file"});
    that.cacheDir = "/media/internal/INPalmbox/.cache";
    
    that.getList = function(path, onSuccess, onFailure)
    {
        _cacheListDepot.get(path, onSuccess, onFailure);
    };
    
    that.addList = function(path, list)
    {
        _cacheListDepot.add(path, list);
    };
    
    that.getFile = function(key, onSuccess, onFailure)
    {
        _cacheFileDepot.get(key, onSuccess, onFailure);
    };
    
    that.addFile = function(key, file)
    {
        _cacheFileDepot.add(key, file);
    };
    
    that.cacheFilesRemove = function(path)
    {
        var self = that;
        _cacheListDepot.get(path,
            function onSuccess(item)
            {
                Mojo.Log.info("cacheFilesRemove");
                if (item)
                {
                    var contents = item.contents;
                    for (var i = 0; i < contents.length; i++)
                    {
                        var content = contents[i];
                        Mojo.Log.info("cacheFilesRemove_is_dir:%s", content.is_dir);
                        if (content.is_dir)
                        {
                            self.cacheFilesRemove(content.path);
                        }
                        else
                        {
                            self.cacheFileRemove(content);
                        }
                    }
                }
            },
            function onFailure()
            {
            }
        );
    };
    
    that.cacheFileRemove = function(file)
    {
        _cacheFileDepot.get(file.hash,
            function onSuccess(item)
            {
                if (item)
                {
                    if (item.ticket)
                    {
                        _deleteDownloadedFile(item.ticket, undefined, undefined);
                    }
                }
            },
            function onFailure()
            {
            }
        );
    };
    
    var _deleteDownloadedFile = function(ticket, onSuccess, onFailure)
    {
        var request = new Mojo.Service.Request("palm://com.palm.downloadmanager/",
        {
            method: "deleteDownloadedFile",
            parameters:
            {
                ticket: ticket
            },
            onSuccess: onSuccess,
            onFailure: onFailure
        });
    };
    
    that.clear = function()
    {
        that.cacheFilesRemove("/");
        
        _cacheFileDepot.removeAll();
        _cacheListDepot.removeAll();
        
        Mojo.Log.info("Cache cleared.");
    };
    
    return that;

}();
