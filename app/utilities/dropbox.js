// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
Dropbox = {};

Dropbox.OAuth = function() {
    
    var that = {};
    
    that.consumerKey = "";
    that.consumerSecret = "";
    that.accessToken = "";
    that.tokenSecret = "";
    
    that.isLoginNecessary = function()
    {
        return (!that.accessToken || !that.tokenSecret);
    };
    
    that.prepareRequest = function(url, options, usePatch)
    {
        if (usePatch === undefined)
        {
            usePatch = true;
        }
        
        options.action = url;
        options.parameters = options.parameters || {};
        
        var accessor =
        {
            consumerSecret: that.consumerSecret, 
            tokenSecret: that.tokenSecret
        };
        
        var params = options.parameters;
        
        params.oauth_signature_method = "HMAC-SHA1";
        params.oauth_consumer_key = that.consumerKey;
        params.oauth_token = that.accessToken;
        
        if (options.method == "POST" && usePatch)
        {
            params._ = "";
        }
        
        OAuth.setTimestampAndNonce(options);
        OAuth.SignatureMethod.sign(options, accessor);
        
        if (options.method == "POST" && usePatch)
        {
            delete params._;
        }
    };
    
    that.request = function(url, options)
    {
        that.prepareRequest(url, options, true);
        Mojo.Log.info("URL:%s", url);
        Mojo.Log.info("key:%s", options.parameters.oauth_consumer_key);
        Mojo.Log.info("token:%s", options.parameters.oauth_token);
        return new Ajax.Request(url, options);
    };
    
    return that;
}();

Dropbox.api = function() {
    
    var that = {};

    var _authServer = "https://api.dropbox.com/{version}/{apiname}";
    var _apiServer = "https://api.dropbox.com/{version}/{apiname}";
    var _contentServer = "https://api-content.dropbox.com/{version}/{apiname}";
    
    var _requestUID = 0;
    var _requestStore = new Hash();
    var _cancelTickets = new Hash();
    
    that.isSandBox = false;
    
    that.login = function(email, password, onSuccess, onFailure)
    {
        Mojo.Log.info("Login requested.");
        Mojo.Log.info("email:%s password:%s", email, password);
        Mojo.Log.info("key:%s secret:%s", Dropbox.OAuth.consumerKey, Dropbox.OAuth.consumerSecret);
        
        Dropbox.OAuth.request(
            _createApiUrl(_authServer, "0", "token"),
            {
                method: "POST",
                parameters:
                {
                    email: email,
                    password: password
                },
                onSuccess: function(transport)
                {
                    var json = transport.responseText.evalJSON();
                    Dropbox.OAuth.accessToken = json.token;
                    Dropbox.OAuth.tokenSecret = json.secret;
                    
                    Mojo.Log.info("Login. token:%s secret:%s", Dropbox.OAuth.accessToken, Dropbox.OAuth.tokenSecret);
                    onSuccess();
                },
                onFailure: function(transport)
                {
                    Mojo.Log.info("Login. Failure");
                    onFailure(transport.status);
                }
            }
        );  
    };
    
    that.account = function(email, first_name, last_name, password, onSuccess, onFailure)
    {
        Dropbox.OAuth.request(
            _createApiUrl(_authServer, "0", "account"),
            {
                method: "POST",
                parameters:
                {
                    email: email,
                    first_name: first_name,
                    last_name: last_name,
                    password: password
                },
                onSuccess: function(transport)
                {
                    if (transport.responseText != "OK")
                    {
                        onFailure(transport.responseText);
                        return;
                    }
                    
                    Mojo.Log.info("Account created.");
                    onSuccess();
                },
                onFailure: function(transport)
                {
                    var json = transport.responseText.evalJSON();
                    onFailure(json.error);
                }
            }
        );   
    };
    
    that.accountInfo = function(onSuccess, onFailure)
    {
        Dropbox.OAuth.request(
            _createApiUrl(_apiServer, "0", "account/info"),
            {
                method: "POST",
                onSuccess: function(transport) {
                    Mojo.Log.info("Account info success.");
                    onSuccess(transport.responseText.evalJSON());
                },
                onFailure: onFailure
            }
        );
    };
    
    that.metadata = function(path, metadata, isList, onSuccess, onFailure)
    {
        var methodName = that.isSandBox ? "metadata/sandbox" : "metadata/dropbox";
        
        Dropbox.OAuth.request(
            _createApiUrl(_apiServer, "0", methodName) + _encodeUri(path),
            {
                method: "POST",
                parameters:
                {
                    hash: (metadata && metadata.hash) ? metadata.hash : undefined,
                    list: isList
                },
                onSuccess: function(transport)
                {
                    var newMetadata = transport.responseText.evalJSON();
                    
                    if (isList)
                    {
                        for (var i = 0; i < newMetadata.contents.length; i++)
                        {
                            newMetadata.contents[i].hash = hex_sha1(Object.toJSON(newMetadata.contents[i]));
                        }
                    }
                    
                    Mojo.Log.info("Meta call success, path: %s", path);
                    onSuccess(newMetadata);
                },
                onFailure: function(resp)
                {
                    Mojo.Log.info("metadata:Failure");
                    onFailure(resp.status);
                }
            }
        );   
    };
    
    var _addRequest = function(request)
    {
        var uid = _requestUID++;
        _requestStore.set(uid, request);
        return uid;
    };
    
    var _releaseRequest = function(uid)
    {
        var item = _requestStore.get(uid);
        if (item)
        {
            _requestStore.unset(uid);
        }
    };
    
    var _upDownloadFallure = function(resp, uid, onFailure)
    {
        if ((resp.aborted !== undefined && resp.aborted)
            || (_isCancelTicket(resp.ticket)))
        {
            _releaseCancelTicket(resp.ticket);
            resp.aborted = true;
        }
        _releaseRequest(uid);
        onFailure(resp);
    };
    
    that.downloadFile = function(srcUrl, destDir, onDownloadSuccess, onDownloadFailure)
    {
        Mojo.Log.info("Download request, src: %s dest: %s", srcUrl, destDir);
        var methodName = that.isSandBox ? "files/sandbox" : "files/dropbox";
        var url = _createApiUrl(_contentServer, "0", methodName) + _encodeUri(srcUrl);
        var options = { method: "GET" };
        
        Dropbox.OAuth.prepareRequest(url, options, false);
        
        var uid = _addRequest(new Mojo.Service.Request("palm://com.palm.downloadmanager/",
            {
                method: "download",
                parameters:
                {
                    target: url + "?" + Object.toQueryString(options.parameters),
                    targetDir: destDir + srcUrl.slice(0, srcUrl.lastIndexOf("/")),
                    targetFilename: srcUrl.slice(srcUrl.lastIndexOf("/") + 1),
                    keepFilenameOnRedirect: true,
                    subscribe: true
                },
                onSuccess: function(resp)
                {
                    if (resp.completionStatusCode && resp.completionStatusCode != 200)
                    {
                        _upDownloadFallure(resp, uid, onDownloadFailure);
                        return;
                    }
                    if (resp.completed)
                    {
                        _releaseRequest(uid);
                    }
                    onDownloadSuccess(resp);
                },
                onFailure: function(e)
                {
                    _upDownloadFallure(e, uid, onDownloadFailure);
                }
            })
        );
    };
    
    that.uploadFile = function (srcFile, destUrl, onUploadSuccess, onUploadFailure)
    {
        Mojo.Log.info("Upload request, src: %s dest: %s", srcFile, destUrl);
        var methodName = that.isSandBox ? "files/sandbox" : "files/dropbox";
        var url = _createApiUrl(_contentServer, "0", methodName) + _encodeUri(destUrl);
        var options = {
            method: "POST",
            parameters:
            {
                file: srcFile.slice(srcFile.lastIndexOf("/") + 1)
            }
        };
        
        Dropbox.OAuth.prepareRequest(url, options, false);
        
        var uid = _addRequest(new Mojo.Service.Request("palm://com.palm.downloadmanager/",
            {
                method: "upload",
                parameters:
                {
                    url: url + "?" + Object.toQueryString(options.parameters),
                    fileName: srcFile,
                    fileLabel: "file",
                    subscribe: true
                },
                onSuccess: function(resp)
                {
                    if ((resp.returnValue !== undefined && !resp.returnValue)
                        || (resp.httpCode !== undefined && resp.httpCode != 200))
                    {
                        _upDownloadFallure(resp, uid, onUploadFailure);
                        return;
                    }
                    if (resp.completed)
                    {
                        _releaseRequest(uid);
                    }
                    onUploadSuccess(resp);
                },
                onFailure: function(e)
                {
                    Mojo.Log.info("Upload failure:%j", e);
                    _upDownloadFallure(e, uid, onUploadFailure);
                }
            })
        );
    };
    
    var _isCancelTicket = function(ticket)
    {
        var item = _cancelTickets.get(ticket);
        return (item === ticket);
    };
    
    var _addCancelTicket = function(ticket)
    {
        _cancelTickets.set(ticket, ticket);
    };
    
    var _releaseCancelTicket = function(ticket)
    {
        var item = _cancelTickets.get(ticket);
        if (item)
        {
            _cancelTickets.unset(ticket);
        }
    };
    
    that.cancelUpDown = function(ticket, onCancelSuccess, onCancelFailure)
    {
        _addCancelTicket(ticket);
        var uid = _addRequest(new Mojo.Service.Request("palm://com.palm.downloadmanager/",
            {
                method: "cancelDownload",
                parameters:
                {
                    ticket: ticket
                },
                onSuccess: function(resp)
                {
                    _releaseRequest(uid);
                    onCancelSuccess(resp);
                },
                onFailure: function(e)
                {
                    _releaseCancelTicket(ticket);
                    _releaseRequest(uid);
                    onCancelFailure(e);
                }
            })
        );
    };
    
    that.deleteFile = function(path, onDeleteSuccess, onDeleteFailure)
    {
        Mojo.Log.info("File delete request");
        
        var rootName = that.isSandBox ? "sandbox" : "dropbox";
        Dropbox.OAuth.request(
            _createApiUrl(_apiServer, "0", "fileops/delete"),
            {
                method: "POST",
                parameters:
                {
                    path: path,
                    root: rootName
                },
                onSuccess: onDeleteSuccess,
                onFailure: onDeleteFailure
            }
        );
    };
    
    that.createFolder = function(path, onMDSuccess, onMDFailure)
    {
        Mojo.Log.info("Create folder request");
        
        var rootName = that.isSandBox ? "sandbox" : "dropbox";
        Dropbox.OAuth.request(
            _createApiUrl(_apiServer, "0", "fileops/create_folder"),
            {
                method: "POST",
                parameters:
                {
                    path: path,
                    root: rootName
                },
                onSuccess: onMDSuccess,
                onFailure: onMDFailure
            }
        );
    };
    
    var _encodeUri = function(uri)
    {
        var path = encodeURIComponent(uri);
        path = path.replace(/%2F/g, "/"); 
        
        // encodeURIComponent() が変換しない記号 !'()*-._~
        path = path.replace(/\!/g, "%21");
        path = path.replace(/\*/g, "%2A");
        path = path.replace(/\"/g, "%27");
        path = path.replace(/\(/g, "%28");
        path = path.replace(/\)/g, "%29");
        
        return path;
    };
    
    var _createApiUrl = function(url, version, apiName)
    {
        url = url.replace("{version}", version);
        url = url.replace("{apiname}", apiName);
        Mojo.Log.info("ApiUrl:%s", url);
        return url;
    };
    
    return that;
}();
