// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var prefs = function() {
    var that = {};
    
    var _cookiePrefs = new Mojo.Model.Cookie("prefs");
    var _data = _cookiePrefs.get();
    var _data = _data || {};
    
    that.getConsumerKey = function()
    {
        return "ここにDropboxで取得した「AppKey」をセット";
    };
    
    that.getConsumerSecret = function()
    {
        return "ここにDropboxで取得した「AppSecret」をセット";
    };
    
    that.getAuth = function()
    {
        var auth = {
            token: "",
            secret: ""
        };
        return _data["auth"] || auth;
    };
    
    that.setAuth = function(token, secret)
    {
        var auth = {
            token: token,
            secret: secret
        };
        Mojo.Log.info("setAuth token:%s secret:%s", auth.token, auth.secret);
        _data["auth"] = auth;
        _cookiePrefs.put(_data);
    };
    
    that.clear = function()
    {
        _cookiePrefs.remove();
    }
    
    return that;
}();
