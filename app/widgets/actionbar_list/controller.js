Mojo.Event.customActionTap = "mojo-customAction-tap";

Mojo.Widget.ActionBarList = Class.create(Mojo.Widget.List, {
    
    initialize: function($super) {
        this.viewsPath = "../widgets/actionbar_list/views/";
        this.autoScroll = false;
    },
    
    setup: function($super) {
        $super();
        
        var actions = this.controller.attributes.actions;
        var renderedActions = [];
        if (actions)
        {
            for(var i = 0; i < actions.length; ++i)
            {
                actions[i].index = i;
                renderedActions.push(Mojo.View.render({
                    object: actions[i],
                    template: this.viewsPath + "action"
                }));
            }
        }
        else
        {
            renderedActions.push(Mojo.View.render({
                object: {index: "0", action: "none", icon: ""},
                template: this.viewsPath + "action"
            }));
        }
        var content = Mojo.View.render({
            object: {actions: renderedActions.join("")},
            template: this.viewsPath + "actionbar"
        });
        
        this.actionBar = Mojo.View.convertToNode(content, this.controller.document);
        
        this.currentActionBar = {
            actionBar: "",
            wrapperDiv: "",
            outerDiv: "",
            contentDiv: "",
            index: -1,
            clear: function() {
                if (this.actionBar) {
                    this.actionBar.remove();
                }
                this.wrapperDiv = "";
                this.outerDiv = "";
                this.contentDiv = "";
                this.index = -1;
            }
        };
        
        this.drawerOpenerOffset = 48; //this.DRAWER_ROW_OFFSET;
        this.drawerBottomOffset = this.drawerOpenerOffset;
        
        this.wasOpen = false;
        
        if (Mojo.Config.animateWithCSS)
        {
            this.doAnimate = this.animateWithCSS;
        }
        else
        {
            this.doAnimate = this.animateWithTimer;
        }
        
        this.controller.exposeMethods(["showActionBar", "hideActionBar", "setActionEnabled", "getActionBarIndex"]);
    },

    handleTap: function($super, event) {
        Mojo.Log.info("handleTap");
        $super(event);
    },
    
    addAsScrollListener: function($super, event) {
        Mojo.Log.info("addAsScrollListener");
        if (!this.autoScroll)
        {
            this.hideActionBar();
        }
        this.autoScroll = false;
        $super(event);
    },
    
    setActionEnabled: function(actionIndex, enabled) {
        var actionDiv = this.actionBar.querySelector("div[id=action_" + actionIndex + "]");
        if (enabled)
        {
            actionDiv.removeClassName("disabled");
        }
        else
        {
            actionDiv.addClassName("disabled");
        }
    },
    
    getActionBarIndex: function() {
        return this.currentActionBar.index;
    },
    
    showActionBar: function(itemIndex) {
        Mojo.Log.info("showActionBar");
        
        if (itemIndex === this.currentActionBar.index)
        {
            return;
        }        
        
        var itemNode = this.getNodeByIndex(itemIndex);
        var self = this;
        function hideCompleteFunc() {
            self.currentActionBar.actionBar = self.actionBar.cloneNode(true);
            self.currentActionBar.actionBar.addEventListener(Mojo.Event.tap, self.handleActionTap.bindAsEventListener(self, self.currentActionBar), false);
            self.currentActionBar.wrapperDiv = self.currentActionBar.actionBar.querySelector("div[id=actionbar-wrapper]")
            self.currentActionBar.outerDiv = self.currentActionBar.actionBar.querySelector("div[id=actionbar-outer]")
            self.currentActionBar.contentDiv = self.currentActionBar.actionBar.querySelector("div[id=actionbar-content]")
            self.currentActionBar.index = itemIndex;
            
            self.listItemsParent.insertBefore(self.currentActionBar.actionBar, itemNode.nextSibling);
            self.updateActionBar(true);
        }
        this.hideActionBar(hideCompleteFunc);
    },
    
    hideActionBar: function(onHideCompleted) {
        Mojo.Log.info("hideActionBar");
        
        if (this.currentActionBar.index < 0)
        {
            if (onHideCompleted)
            {
                onHideCompleted();
            }
            return;
        }
        
        var self = this;
        function completeFunc() {
            self.currentActionBar.clear();
            if (onHideCompleted)
            {
                onHideCompleted();
            }
        }
        this.updateActionBar(false, completeFunc);
    },
    
    handleActionTap: function(event, currentActionBar) {
        Mojo.Log.info("handleActionTap");
        
        var itemIndex = currentActionBar.index;
        if (itemIndex < 0)
        {
            return;
        }
        
        var modelItem = this.listItems[itemIndex];
        if (modelItem === undefined)
        {
            return;
        }
        
		Event.stop(event);

		var actionNode = Mojo.View.findParentByAttribute(event.target, undefined, "action");
        if (actionNode.className.search(/disabled/i) !== -1)
        {
            return;
        }
		var actionAttr = actionNode && actionNode.getAttribute("action");
        if (!actionAttr)
        {
            return;
        }
        
        Mojo.Event.send( this.controller.element,
            Mojo.Event.customActionTap, 
			{
                model: this.controller.model,
                item: modelItem,
                index: itemIndex + this.renderOffset,
                originalEvent: event,
                action: actionAttr
            }
        );
    },
    
    dragStartHandler: function($super, event) {
        if ((this.controller.attributes.swipeToDelete)
            && (Math.abs(event.filteredDistance.x) > 2*Math.abs(event.filteredDistance.y)))
        {
                this.hideActionBar();
                $super(event);
        }
        else
        {
            $super(event);
        }
    },
    
// これより下は Drawer 内部ソース流用（一部修正あり）
    
    _updateScrollPosition: function(scroller, origScrollerHeight, pos) {
        if (scroller.mojo.scrollerSize().height === origScrollerHeight)
        {
            scroller.mojo.setScrollPosition({y:-pos});
        }
        else
        {
            this.scrollPosAnimator.cancel();
        }
    },

    scrollIntoView: function(elementHeight) {
        var scrollToPos;
        
        var scroller = Mojo.View.getScrollerForElement(this.controller.element);
        var element = this.currentActionBar.actionBar; //this.controller.element;
        
        var currentTop = -scroller.mojo.getScrollPosition().top;
        var scrollerHeight = scroller.mojo.scrollerSize().height;
        var contentHeight = scroller.scrollHeight;
        var maxScrollPos = contentHeight - scrollerHeight;
		
        var currentBottom = currentTop + scrollerHeight;
        
        var elementOffset = Element.positionedOffset(element);
        
        var currentlyShowing = currentBottom - elementOffset.top;
        var remainingToShow = elementHeight - currentlyShowing;
        
        var newTop = currentTop + remainingToShow;
        
        var newContentBottom = newTop + scroller.mojo.scrollerSize().height;
        var newContentTop = newContentBottom - elementHeight;
        
        var openerAdjust = this.drawerOpenerOffset;
        
        newContentTop -= openerAdjust;
        
        if (openerAdjust + elementHeight > scrollerHeight)
        {
            scrollToPos = newContentTop;
        }
        else if ((newContentBottom + this.drawerBottomOffset) > currentBottom)
        {
            scrollToPos = newTop + this.drawerBottomOffset;
        }
        else if (newContentTop < currentTop)
        {
            scrollToPos = newContentTop;
        }
        
        if (scrollToPos)
        {
            if (scrollToPos > maxScrollPos)
            {
                var details = {
                    from: currentTop,
                    to: scrollToPos
                };
                this.scrollPosAnimator = Mojo.Animation.animateValue(Mojo.Animation.queueForElement(this.controller.element),'zeno',this._updateScrollPosition.bind(this,scroller,scrollerHeight),details);
            }
            else
            {
                this.autoScroll = true;
                scroller.mojo.scrollTo(undefined, -scrollToPos, true);
            }
        }
    },

    updateActionBar: function(isOpen, completeFunc) {
        var newHeight;
        var currentValue = 0;
        var scroller;
        var drawerHeight;
        
        if (this.wasOpen !== isOpen)
        {
            this.wasOpen = isOpen;
            newHeight = this.currentActionBar.contentDiv.offsetHeight;
            scroller = Mojo.View.getScrollerForElement(this.controller.element);
            
            if (!isOpen)
            {
                currentValue = newHeight;
            }
            drawerHeight = (isOpen && newHeight) || 0;
            if (isOpen)
            {
                this.scrollIntoView(drawerHeight);
            }
            this.doAnimate(newHeight, currentValue, scroller, drawerHeight, completeFunc);
        }
    },

    animateWithTimer: function(newHeight, currentValue, scroller, drawerHeight, completeFunc){
        var that = this;
        function completionFunction(scrollerHeight) {
            that.animationComplete(scroller, scrollerHeight, drawerHeight);
            
            if (completeFunc)
            {
                completeFunc();
            }
        }
        Mojo.Animation.animateStyle(this.currentActionBar.wrapperDiv, "height", "bezier",
            {from:0,to:newHeight,duration:0.11,currentValue:currentValue,reverse:!this.wasOpen,onComplete:completionFunction.bind(this,scroller.mojo.scrollerSize().height),curve:'ease-in-out'});
    },
    
    
    animateWithCSS: function(newHeight, currentValue, scroller, drawerHeight, completeFunc){
        var that = this;
        var wrapper = that.currentActionBar.wrapperDiv;
        var curriedCompletionFunction;
        function completionFunction(scrollerHeight, webkitTransitionEndEvent) {
            that.animationComplete(scroller, scrollerHeight, drawerHeight);
            
            if (completeFunc)
            {
                completeFunc();
            }
        }
        curriedCompletionFunction = completionFunction.curry(scroller.mojo.scrollerSize().height);
        Mojo.Animation.animateStyleWithCSS(wrapper,
            {property:"height",duration:0.11,to:drawerHeight+"px",setToComputed:true},
            curriedCompletionFunction);
    },
    
    
    animationComplete: function(scroller, origHeight, drawerHeight, el, cancelled){
        if (!cancelled)
        {
            Mojo.Widget.Scroller.validateScrollPositionForElement(this.controller.element);
            if (origHeight !== scroller.mojo.scrollerSize().height)
            {
                this.scrollIntoView(drawerHeight);
            }
            if (this.wasOpen)
            {
                this.currentActionBar.wrapperDiv.style.height = "auto";
            }
        }
    }

});