// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var DropboxList = Class.create({
    
    initialize: function(path)
    {
        var self = this;
        var fixPath = function(path)
        {
            if (path === null
                || path === undefined
                || typeof path != "string")
            {
                return "/";
            }
            
            if (path.charAt(0) !== "/")
            {
                path = "/" + path;
            }
            if (1 < path.length && path.charAt(path.length-1) === "/")
            {
                path = path.slice(0, path.length-1);
            }
            
            return path;
        };
        
        var setPath = function(path)
        {
            self.path = fixPath(path);
            if (self.path === "/")
            {
                self.title = $L("My Dropbox");
            }
            else
            {
                self.title = self.path.slice(self.path.lastIndexOf("/") + 1);
            }
        };
        
        this.title = $L("My Dropbox");
        this.path = "/";
        this.contents = [];
        setPath(path);
        
    },
    
    _compreContent: function(compA, compB)
    {
        if (compA.is_dir === compB.is_dir)
        {
            if (compA.name === compB.name) return 0;
            
            var strs = new Array(compA.name, compB.name);
            strs.sort();
            return strs[0] === compA.name ? -1 : 1;
        }
        else
        {
            return compA.is_dir ? -1 : 1; 
        }
    },
    
    setContents: function(contents)
    {
        this.contents = contents;
        this.contents.sort(this._compreContent)
    },
    
    refresh: function(isCache, onFillStart, onFillSuccess, onFillFailure)
    {
        var self = this;
        this._downloadListCache(isCache, this.path,
            function onStart()
            {
                onFillStart();
            },
            function onSuccess(metadata)
            {
                self.setContents(metadata.contents);
                onFillSuccess();
            },
            function onFailure(status)
            {
                Mojo.Log.info("fillContents_onFailure:%s", status);
                if(status == 304)
                {
                    // 変更なし
                    onFillSuccess();
                }
                else
                {
                    onFillFailure();
                }
            }
        );
    },
    
    _downloadListCache: function(isCache, path, onStart, onSuccess, onFailure)
    {
        var self = this;
        if (isCache)
        {
            INPalmBox.cacheManager.getList(path,
                function onGet(item)
                {
                    if (item)
                    {
                        onSuccess(item);
                    }
                    onStart();
                    self._downloadList(path, onSuccess, onFailure);
                },
                function onNotGet()
                {
                    onStart();
                    self._downloadList(path, onSuccess, onFailure);
                }
            );
        }
        else
        {
            onStart();
            self._downloadList(path, onSuccess, onFailure);
        }
    },

    _downloadList: function(path, onSuccess, onFailure)
    {
        Dropbox.api.metadata(path, undefined, true,
            function onMetadataSuccess(metadata)
            {
                var contents = metadata.contents;
                for (var i = 0; i < contents.length; i++)
                {
                    var item = contents[i];
                    item.name = item.path.slice(item.path.lastIndexOf("/") + 1);
                }
                INPalmBox.cacheManager.addList(path, metadata);
                onSuccess(metadata);
            },
            function onMetadataFailure(status)
            {
                onFailure(status);
            }
        );
    },
    
    deleteItem: function(item)
    {
        var self = this;
        Dropbox.api.deleteFile(item.path, 
            function onSuccess()
            {
                var path = item.path;
                if (item.is_dir)
                {
                    INPalmBox.cacheManager.cacheFilesRemove(path);
                }
                else
                {
                    INPalmBox.cacheManager.cacheFileRemove(item);
                }
                self._downloadList(self.path,
                    function onSuccess(resp)
                    {
                        self.setContents(resp.contents);
                    });
            },
            function onFailure()
            {
            }
        );
    },
    
    createFolder: function(newFolderName, onMDSuccess, onMDFailure)
    {
        Dropbox.api.createFolder(this.path + "/" + newFolderName,
            function onSuccess()
            {
                onMDSuccess();
            },
            function onFailure()
            {
                onMDFailure();
            }
        );
    }
});
