// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var UploadList = Class.create({
    
    initialize: function(path)
    {
        this.path = path;
        this.contents = [];
    },
    
    hasContents: function()
    {
        return (0 < this.contents.length);
    },
    
    refresh: function()
    {
        this.contents = uploadManager.getUploadList(this.path);
    }
    
});
