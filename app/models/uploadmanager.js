// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var uploadManager = function() {
    
    var that = {};
    
    // ダウンロードリスト
    var _uploaders = new Hash();
    
    that.hasUploader = function(key)
    {
        var loader = _uploaders.get(key);
        return (loader !== undefined);
    };
    
    that.getUploader = function(key, item)
    {
        var loader = _uploaders.get(key);
        if (loader)
        {
            return loader;
        }
        else if (item)
        {
            if (!item.uploadUrl || !item.fullPath) return null;
            
            loader = new Uploader(item);
            this.addUploader(loader);
            return loader;
        }
        else
        {
            return null;
        }
    };
    
    that.getUploadList = function(path)
    {
        var list = [];
        var i = 0;
        var url = path;
        if (1 < url.length && url.charAt(url.length-1) != "/")
        {
            url += "/";
        }
        
        _uploaders.each(function(pair)
        {
            var loader = pair.value;
            if (loader.uploadInfo.url === url)
            {
                list[i] = loader.uploadInfo;
                i++;
            }
        });
        
        return list;
    };
    
    that.addUploader = function(loader)
    {
        _uploaders.set(loader.key, loader);
    };

    that.releaseUploader = function(key)
    {
        var loader = _uploaders.get(key);
        if (loader)
        {
            _uploaders.unset(key);
        }
    };
    
    that.cancelAllUploads = function()
    {
        var loaders = _uploaders.values();
        for (var i = 0; i < loaders.length; i++)
        {
            loaders[i].cancelUpload();
        }
    };
    
    return that;
}();