// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var downloadManager = function() {
    
    var that = {};
    
    // ダウンロードリスト
    var _downloaders = new Hash();
    
    that.downloadDir = "/media/internal/INPalmbox/download",
    
    that.hasDownloader = function(key)
    {
        var loader = _downloaders.get(key);
        return (loader !== undefined);
    };
    
    that.getDownloader = function(key, item)
    {
        var loader = _downloaders.get(key);
        if (loader)
        {
            return loader;
        }
        else if (item)
        {
            if (item.is_dir || !item.path) return null;
            
            loader = new Downloader(item);
            this.addDownloader(loader);
            return loader;
        }
        else
        {
            return null;
        }
    };
    
    that.addDownloader = function(loader)
    {
        _downloaders.set(loader.key, loader);
    };

    that.releaseDownloader = function(key)
    {
        var loader = _downloaders.get(key);
        if (loader)
        {
            _downloaders.unset(key);
        }
    };
    
    that.getCacheInfo = function(item, onSuccess, onFailure)
    {
        if (item.is_dir || !item.path) return;
        
        var fileInfo =
        {
            cached: false,
            ticket: null,
            target: null,
            mime_type: null
        };
        
        var self = this;
        INPalmBox.cacheManager.getFile(item.hash,
            function onGet(item)
            {
                if (item)
                {
                    fileInfo.cached = true;
                    fileInfo.target = item.target;
                    fileInfo.ticket = item.ticket;
                    fileInfo.mime_type = item.mime_type;
                    
                    onSuccess(fileInfo);
                }
                else
                {
                    onFailure();
                }
            },
            function onNotGet()
            {
                onFailure();
            }
        );
    };
    
    that.cancelAllDownloads = function()
    {
        var loaders = _downloaders.values();
        for (var i = 0; i < loaders.length; i++)
        {
            loaders[i].cancelDownload();
        }
    };
    
    return that;
}();