// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var Uploader = Class.create({

    initialize: function(item)
    {
        this.uploadInfo = {
            key: "",
            date: (new Date()).getTime(),
            url: "",
            fullPath: "",
            filePath: "",
            name: "",
            ticket: null
        };
        
        this._handleUploadStart = this._onUploadStart.bind(this),
        this._handleUploading = this._onUploading.bind(this),
        this._handleUploaded = this._onUploaded.bind(this),
        this._handleUploadFailure = this._onUploadFailure.bind(this),
        
        this.key = "";
        this._item = item;
        var uploadUrl = item.uploadUrl;
        if (1 < uploadUrl.length && uploadUrl.charAt(uploadUrl.length-1) != "/")
        {
            uploadUrl += "/";
        }

Mojo.Log.info("Uploader:%s", uploadUrl);
        this.uploadInfo.key = this.key = item.hash;
        this.uploadInfo.url = uploadUrl;
        this.uploadInfo.fullPath = item.fullPath;
        this.uploadInfo.filePath = item.fullPath.slice(0, item.fullPath.lastIndexOf("/"));
        this.uploadInfo.name = item.fullPath.slice(item.fullPath.lastIndexOf("/") + 1);
        
        this.uploadStartEvent;
        this.uploadingEvent;
        this.uploadedEvent;
        this.uploadFailureEvent;
    },
    
    getItem: function()
    {
        return this._item;
    },
    
    uploadFile: function()
    {
        var srcFile = this.uploadInfo.fullPath;
        var destUrl = this.uploadInfo.url;
        
        var self = this;
        var isStart = true;
        Dropbox.api.uploadFile(srcFile, destUrl,
            function onFileUploadSuccess(resp)
            {
                self.uploadInfo.ticket = resp.ticket;
                
                if (isStart)
                {
                    self._handleUploadStart();
                    isStart = false;
                }
                self._handleUploading();
                
                if (resp.completed)
                {
                    self._handleUploaded();
                }
            },
            function onFileUploadFailure(resp)
            {
                self._handleUploadFailure(resp);
            }
        );
        
    },
    
    cancelUpload: function()
    {
        if (this.uploadInfo.ticket)
        {
            Dropbox.api.cancelUpDown(this.uploadInfo.ticket);
        }
    },
    
    _onUploadStart: function()
    {
        if (this.uploadStartEvent)
        {
            this.uploadStartEvent(this, this.uploadInfo);
        }
    },
    
    _onUploading: function()
    {
        if (this.uploadingEvent)
        {
            this.uploadingEvent(this, this.uploadInfo);
        }
    },
    
    _onUploaded: function()
    {
        uploadManager.releaseUploader(this.uploadInfo.key);
        
        if (this.uploadedEvent)
        {
            this.uploadedEvent(this, this.uploadInfo);
        }
        else
        {
            Mojo.Controller.getAppController().showBanner($L("Uploaded: ") + this.uploaderInfo.name, null);
        }
    },
    
    _onUploadFailure: function(resp)
    {
        uploadManager.releaseUploader(this.uploadInfo.key);
        
        if (this.uploadFailureEvent)
        {
            this.uploadFailureEvent(this, this.uploadInfo, resp);
        }
        else
        {
            Mojo.Controller.getAppController().showBanner($L("Up failed: ") + this.uploaderInfo.name, null);
        }
    }
});
