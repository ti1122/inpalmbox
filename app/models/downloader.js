// --------------------------------------------------------------------------
// Copyright (c) 2010 ti1122
// License: The MIT License (http://opensource.org/licenses/mit-license.php)
// --------------------------------------------------------------------------
var Downloader = Class.create({
    
    initialize: function(item)
    {
        this.downloadInfo = {
            key: "",
            name: "",
            cached: false,
            ticket: null,
            receivedBytes: 0,
            totalBytes: 0,
            target: null,
            mime_type: "",
            isCache: false,
            progressValue: 0
        };
        
        this._handleDownloadStart = this._onDownloadStart.bind(this),
        this._handleDownloading = this._onDownloading.bind(this),
        this._handleDownloaded = this._onDownloaded.bind(this),
        this._handleDownloadFailure = this._onDownloadFailure.bind(this),
        
        this.key = "";
        this._item = item;
        this.downloadInfo.key = this.key = item.hash;
        this.downloadInfo.name = item.name;
        this.downloadInfo.mime_type = item.mime_type;
        this.downloadInfo.totalBytes = item.bytes;
        
        this.downloadStartEvent;
        this.downloadingEvent;
        this.downloadedEvent;
        this.afterDownloadEvent;
        this.downloadFailureEvent;
    },
    
    getItem: function()
    {
        return this._item;
    },
    
    downloadFile: function(isCache)
    {
        if (this._item.is_dir || !this._item.path)
        {
            this._onDownloadFailure();
            return;
        }
        
        this.downloadInfo.isCache = isCache;
        var dlDir = isCache ? INPalmBox.cacheManager.cacheDir : downloadManager.downloadDir;
        
        var self = this;
        var isStart = true;
        Dropbox.api.downloadFile(this._item.path, dlDir,
            function onFileDownLoading(resp)
            {
                self.downloadInfo.ticket = resp.ticket;
                self.downloadInfo.receivedBytes = resp.amountReceived ? resp.amountReceived : self.downloadInfo.receivedBytes;
                if (isStart)
                {
                    self._handleDownloadStart();
                    isStart = false;
                }
                self.downloadInfo.progressValue = self.downloadInfo.receivedBytes / self.downloadInfo.totalBytes;
                
                self._handleDownloading();
                if (resp.completed)
                {
                    Mojo.Log.info("Completed");
                    if (isCache)
                    {
                        var cacheItem = {
                            target: resp.target,
                            ticket: resp.ticket,
                            mime_type: self.downloadInfo.mime_type
                        };
                        INPalmBox.cacheManager.addFile(self._item.hash, cacheItem);
                    }
                    
                    self.downloadInfo.progressValue = 1;
                    self.downloadInfo.ticket = null;
                    self.downloadInfo.target = resp.target;
                    self._handleDownloaded();
                }
            },
            function onFileDownloadFailure(resp)
            {
                self._handleDownloadFailure(resp);
            }
        );
        
    },
    
    cancelDownload: function()
    {
        if (this.downloadInfo.ticket)
        {
            Dropbox.api.cancelUpDown(this.downloadInfo.ticket);
        }
    },
    
    _onDownloadStart: function()
    {
        if (this.downloadStartEvent)
        {
            this.downloadStartEvent(this, this.downloadInfo);
        }
    },
    
    _onDownloading: function()
    {
        if (this.downloadingEvent)
        {
            this.downloadingEvent(this, this.downloadInfo);
        }
    },
    
    _onDownloaded: function()
    {
        downloadManager.releaseDownloader(this.downloadInfo.key);
        if (this.downloadedEvent)
        {
            this.downloadedEvent(this, this.downloadInfo);
            
            if (this.afterDownloadEvent)
            {
                var self = this;
                setTimeout(function() {
                    self.afterDownloadEvent(self, self.downloadInfo);
                }, 1000);
            }
        }
        else
        {
            if (this.downloadInfo.isCache)
            {
                Mojo.Controller.getAppController().showBanner($L("Cached: ") + this.downloadInfo.name, null);
            }
            else
            {
                Mojo.Controller.getAppController().showBanner($L("Downloaded: ") + this.downloadInfo.name, null);
            }
        }
    },
    
    _onDownloadFailure: function(resp)
    {
        downloadManager.releaseDownloader(this.downloadInfo.key);
        if (this.downloadFailureEvent)
        {
            this.downloadFailureEvent(this, this.downloadInfo, resp);
        }
        else
        {
            Mojo.Controller.getAppController().showBanner($L("DL failed: ") + this.downloadInfo.name, null);
        }
    }
    
});
